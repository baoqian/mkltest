PROJECT_ROOT = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

all:
	make -f $(PROJECT_ROOT)src/tests/Makefile all

clean:
	make -f $(PROJECT_ROOT)src/tests/Makefile clean