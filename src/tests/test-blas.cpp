/*
 * Timer.h
 *
 *  Created on: 20240509
 *      Author: baoqian
 */

#include "Timer.h"
#include <mkl.h>
#include <vector>
#include <iostream>

namespace {

static int test(int m, int n, int common_dim) {
	std::vector<float> A(m * common_dim);
	std::vector<float> B(common_dim * n);
	std::vector<float> C(m * n);


	for (int i = 0; i < m; ++i) {
		for (int k = 0; k < common_dim; ++k) {
			const unsigned r = std::rand();
			A[i * common_dim + k] = (r % 128) * 1.0;
		}
	}

	for (int j = 0; j < n; ++j) {
		for (int k = 0; k < common_dim; ++k) {
			B[k * n + j] = std::rand() * 1.0;
		}
	}


	const auto k = common_dim;
	const float alpha = 1.0, beta = 0.0;
	{
		Timer timer(n);
		timer << "m=" << m << ", n=" << n << ", k=" << k;
		cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, A.data(), k, B.data(), n, beta, C.data(), n);
	}

	// for (int i = 0; i < m; ++i) {
	// 	for (int j = 0; j < n; ++j) {
	// 		float sum = 0;
	// 		for (int k = 0; k < common_dim; ++k) {
	// 			sum += A[i * common_dim + k] * B[k * n + j];
	// 		}
	// 		std::cout << sum << " v.s. " << C[i * n + j] << std::endl;
	// 	}
	// }
	return 0;
}

} /* namespace */

int main(int argc, const char *argv[]) {
	test(11008, 1, 4096);

	test(4096, 1, 11008);

	test(4096, 1, 4096);

	
	return 0;
}

