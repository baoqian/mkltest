/*
 * Timer.h
 *
 *  Created on: 20240509
 *      Author: baoqian
 */

#ifndef SRC_TESTS_TIMER_H_
#define SRC_TESTS_TIMER_H_

#include <chrono>
#include <iostream>
#include <sstream>

class Timer {
public:
	Timer(const std::string &header, int average_count,
			const std::string &end = ".\n") : average_count_(average_count), end_(end) {
		strout_ << header;
		start_ = std::chrono::high_resolution_clock::now();
	}

	Timer(const std::string &header, const std::string &end = ".\n") : Timer(header, 1, end) {}
	Timer(int average_count = 1, const std::string &end = ".\n") : Timer("", average_count, end) {}

	~Timer() {
		const auto end = std::chrono::high_resolution_clock::now();
		const std::chrono::duration<double, std::milli> ms = end - start_;
		strout_ << " " << ms.count() << " ms";
		if (average_count_ > 1) {
			strout_ << ", average " << ms.count() / average_count_ << " ms";
		}
		strout_ << end_;
		std::cout << strout_.str();
	}

	template<class T>
	Timer &operator<<(const T &msg) {
		strout_ << msg;
		start_ = std::chrono::high_resolution_clock::now();
		return *this;
	}

	int AverageCount() const { return average_count_; }

private:
	int average_count_;
	std::string end_;
	std::stringstream strout_;
	std::chrono::high_resolution_clock::time_point start_;
};

#endif /* SRC_TESTS_TIMER_H_ */

